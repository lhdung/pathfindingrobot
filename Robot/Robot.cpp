// Robot.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Robot.h"
#include "Line.h"
#include "Polygon.h"
#include "Map.h"
#include "Algorithm.h"
#include <time.h>
UINT MyTimeOut = 20*1*1000;
#define MAX_LOADSTRING 100
bool isSetSource = false;
HWND hWaitDlg{};
HANDLE thread{};
time_t start;
time_t end;
struct THREAD_PARMS {
	HWND hWnd;
	Map* map;
	UINT algo;
};
DWORD WINAPI AlgoThread(LPVOID lParam)
{
	if(((THREAD_PARMS*)lParam)->algo == 1)
	{
		Map::Run(((THREAD_PARMS*)lParam)->hWnd, *((THREAD_PARMS*)lParam)->map, Algorithm::IDAStar);
	}
	else if (((THREAD_PARMS*)lParam)->algo == 2)
	{
		Map::Run(((THREAD_PARMS*)lParam)->hWnd, *((THREAD_PARMS*)lParam)->map, Algorithm::RecurBestFirstSearch);

	}
	else if (((THREAD_PARMS*)lParam)->algo == 3)
	{
		Map::Run(((THREAD_PARMS*)lParam)->hWnd, *((THREAD_PARMS*)lParam)->map, Algorithm::AStar);

	}
	EndDialog(hWaitDlg, TRUE);
	return TRUE;
}
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
Map map;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK EditDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK WaitDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_ROBOT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}
	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_ROBOT));

	MSG msg;
	
	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ROBOT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_ROBOT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_LOADMAP:
		{
			OPENFILENAMEA ofn{};
			ofn.lStructSize = sizeof(OPENFILENAME);
			CHAR buffer[512] = {};
			ofn.lpstrFilter = "Map Data\0*.txt\0";
			ofn.nFilterIndex = 1;
			ofn.lpstrFile = buffer;
			ofn.hwndOwner = hWnd;
			ofn.nMaxFile = sizeof(buffer);
			ofn.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;
			if (GetOpenFileNameA(&ofn))
			{
				map.clearPath();
				map.loadMap(buffer);
				map.findMaxPoint();
				map.findScalingRatio(hWnd);
				InvalidateRect(hWnd, NULL, TRUE);
			}
			break;
		}
		case ID_EDIT_SETSOURCEPOINT:
		{
			isSetSource = true;
			DialogBox(hInst, MAKEINTRESOURCE(IDD_INPUTPOINT), hWnd, EditDlg);
			map.clearPath();
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		case ID_EDIT_SETDESTPOINT:
		{
			isSetSource = false;
			DialogBox(hInst, MAKEINTRESOURCE(IDD_INPUTPOINT), hWnd, EditDlg);
			map.clearPath();
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		case ID_ASTAR:
		{
			THREAD_PARMS thrInfo;
			thrInfo.algo = 3;
			thrInfo.hWnd = hWnd;
			thrInfo.map = &map;
			start = clock();
			thread = CreateThread(NULL, 0, AlgoThread, (LPVOID)&thrInfo, 0,0);
			DialogBox(hInst, MAKEINTRESOURCE(IDD_WAITING), hWnd, WaitDlgProc);
			end = clock();
			if (WaitForSingleObject(thread, MyTimeOut + 500 - (end-start)) == WAIT_TIMEOUT)
			{
				TerminateThread(thread, 0);
				CloseHandle(thread);
				thread = NULL;
				MessageBoxA(hWnd, "Timeout!!! Algorithm is too slow!!!", "Error", MB_OK | MB_ICONERROR);
			}
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		case ID_RBFSEARCH:
		{
			THREAD_PARMS thrInfo;
			thrInfo.algo = 2;
			thrInfo.hWnd = hWnd;
			thrInfo.map = &map;
			start = clock();

			thread = CreateThread(NULL, 0, AlgoThread, (LPVOID)&thrInfo, 0, 0);
			DialogBox(hInst, MAKEINTRESOURCE(IDD_WAITING), hWnd, WaitDlgProc);
			end = clock();

			if (WaitForSingleObject(thread, MyTimeOut + 500 - (end - start)) == WAIT_TIMEOUT)
			{
				TerminateThread(thread, 0);
				CloseHandle(thread);
				thread = NULL;
				MessageBoxA(hWnd, "Timeout!!! Algorithm is too slow!!!", "Error", MB_OK | MB_ICONERROR);
			}
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		case ID_IDASTAR:
		{
			THREAD_PARMS thrInfo;
			thrInfo.algo = 1;
			thrInfo.hWnd = hWnd;
			thrInfo.map = &map;
			start = clock();

			thread = CreateThread(NULL, 0, AlgoThread, (LPVOID)&thrInfo, 0, 0);
			DialogBox(hInst, MAKEINTRESOURCE(IDD_WAITING), hWnd, WaitDlgProc);
			end = clock();

			if (WaitForSingleObject(thread, MyTimeOut + 500 - (end - start)) == WAIT_TIMEOUT)
			{
				TerminateThread(thread, 0);
				CloseHandle(thread);
				thread = NULL;
				MessageBoxA(hWnd, "Timeout!!! Algorithm is too slow!!!", "Error", MB_OK | MB_ICONERROR);
			}
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		
		case ID_REFRESH:
		{
			map.clearPath();
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		map.drawCoordinate(hWnd);
		map.draw(hWnd);
		map.drawSolution(hWnd);
		EndPaint(hWnd, &ps);
		break;

	}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
INT_PTR CALLBACK WaitDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		hWaitDlg = hDlg;
		SetTimer(hDlg, 1, MyTimeOut, NULL);
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			
		}
		break;
	case WM_TIMER:
	{
		KillTimer(hDlg, 1);
		EndDialog(hDlg,TRUE);
		break;
	}
	}
	
	return (INT_PTR)FALSE;
}
INT_PTR CALLBACK EditDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			if (LOWORD(wParam) == IDOK)
			{
				char buffer[8] = {};
				char buffer2[8] = {};
				GetWindowTextA(GetDlgItem(hDlg, IDC_EDITX), buffer, 8);
				GetWindowTextA(GetDlgItem(hDlg, IDC_EDITY), buffer2, 8);
				if (buffer[0] != '\0' && buffer2[0] != '\0')
				{
					MYPOINT pt;
					pt.x = atoi(buffer);
					pt.y = atoi(buffer2);
					if (isSetSource)
					{
						map.setSource(pt);
					}
					else
					{
						map.setDest(pt);
					}
				}
				isSetSource = false;
				
			}
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}