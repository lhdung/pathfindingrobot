#include "stdafx.h"
#include "Polygon.h"
POLYGON::POLYGON()
{
}


// Reserve numVert elements in verticle list
POLYGON::POLYGON(int numVert)
{
	_vertList.reserve(numVert);
}

POLYGON::POLYGON(std::initializer_list<POLYPOINT*> pointList)
{
	_vertList.reserve(pointList.size());
	for (auto point : pointList)
		_vertList.push_back(point);
}

POLYGON::~POLYGON()
{
	for (auto vert : _vertList)
		delete vert;
}

void POLYGON::addVert(POLYPOINT * p)
{
	_vertList.push_back(p);
}

bool POLYGON::isCrossed(const LineSegment & line)
{
	return false;
}

int POLYGON::getNumVert()
{
	return _vertList.size();
}

POLYPOINT POLYGON::getPoint(int vertTag)
{
	return *_vertList[vertTag];
}

bool POLYGON::isPtInside(const MYPOINT & pt)
{
	bool isInside = false;
	for (int i = 0, j = this->_vertList.size() - 1;i < _vertList.size(); j = i++)
	{
		if ((_vertList[i]->_point.y <= pt.y && _vertList[j]->_point.y >= pt.y)
			|| (_vertList[i]->_point.y >= pt.y && _vertList[j]->_point.y <= pt.y))
		{
			auto slope = (_vertList[j]->_point.x - _vertList[i]->_point.x) / (_vertList[j]->_point.y - _vertList[i]->_point.y);
			auto ptX = _vertList[i]->_point.x + slope*(pt.y - _vertList[i]->_point.y);
			if (compare(ptX, pt.x) == -1)
			{
				isInside = !isInside;
			}
			else if (compare(ptX, pt.x) == 0)
				return false;
		}
	}
	return isInside;
}

TAG::TAG()
{
}

TAG::TAG(int pPolyTag, int pVertTag): polyTag(pPolyTag), vertTag(pVertTag)
{
}

POLYPOINT::POLYPOINT()
{
}

POLYPOINT::POLYPOINT(double pX, double pY, int pPolyTag, int pVertTag): _point(pX, pY), _tag(pPolyTag, pVertTag)
{
}

POLYPOINTUCS::POLYPOINTUCS()
{
}

POLYPOINTUCS::POLYPOINTUCS(double pX, double pY, int pPolyTag, int pVertTag, double pPathCost, TAG pParentTag) :
	POLYPOINT(pX, pY, pPolyTag, pVertTag), pathCost(pPathCost), parentTag(pParentTag)

{
}


