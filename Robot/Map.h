#pragma once
#include "stdafx.h"
#include "Polygon.h"
#define RATIO 0.1
#define PADDING 10
#define COOR_ARROW_X 20
#define COOR_ARROW_Y 10
#define LENGTH 5
#define POINT_RAD 4
class Map;
using MyAlgorithm = int(*)(Map& map,double& distance,std::vector<POLYPOINT*>& path);
class Map
{
public:
	Map();
	~Map();
	bool loadMap(const char* filename);
	bool setSource(const MYPOINT& src);
	bool setDest(const MYPOINT& dest);
	static void Run(HWND,Map& map, MyAlgorithm algo);
	std::shared_ptr<std::list<TAG>> generateNodes(POLYPOINT* node) const;
	void addPolygon(POLYGON* polygon);
	void updateConnectedListSize(); // call this function after add all polygon to _polyList (using addPolygon()) to update _connectedList's size
	POLYPOINT getDest() const;
	POLYPOINT getSource() const;
	POLYPOINT getPoint(int pPolyTag, int pVertTag);
	POLYPOINT* getPointPtr(int pPolyTag, int pVertTag);
	BOOL isReachDest();
	void draw(HWND hWnd);
	void drawSolution(HWND hWnd);
	void drawCoordinate(HWND hWnd);
	void findMaxPoint(); //This function is used to find maxX and maxY after the map is loaded
	void findScalingRatio(HWND hWnd); //This function will used to find scaling ratio
	void clearPath();
private:
	std::vector< POLYGON*> _polyList;
	std::vector<std::vector<std::shared_ptr<std::list<TAG>>>> _connectedList;
	//If the source is not polygon vertex, its polyTag = the numbers of Polygon, vertTag = 0
	//Otherwise its tag = polygon vertex tag
	POLYPOINT _src;
	POLYPOINT _dest;
	std::vector<POLYPOINT*> path;
public:
	double distance;
	bool isValidSource{true};
	bool isValidDest{ true };
	double execTime = 0;
	//Max X and Max Y from the map.
	//Use this to calculate ratio between the map and the screen so we can fit the map to the client area
	int maxX{ 100 };
	int maxY{ 100 };
	double scalingRatio{ 5 };
	friend class Algorithm;
};