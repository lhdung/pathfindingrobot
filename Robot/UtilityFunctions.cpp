#include "stdafx.h"

int CrossProduct(const MYPOINT & a, const MYPOINT & b, const MYPOINT & c, const MYPOINT & d)
{
	int vectXa = b.x - a.x;
	int vectYa = b.y - a.y;

	int vectXb = d.x - c.x;
	int vectYb = d.y - c.y;
	return vectXa*vectYb - vectYa*vectXb;
}

int DotProduct(const MYPOINT & a, const MYPOINT & b, const MYPOINT & c, const MYPOINT & d)
{
	int vectXa = b.x - a.x;
	int vectYa = b.y - a.y;

	int vectXb = d.x - c.x;
	int vectYb = d.y - c.y;
	return vectXa*vectXb + vectYa*vectYb;
}

double Distance(const MYPOINT & a, const MYPOINT & b)
{
	return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}
