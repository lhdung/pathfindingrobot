#pragma once
#include "stdafx.h"
struct TAG
{
	int polyTag;
	int vertTag;
	TAG();
	TAG(int pPolyTag, int pVertTag);
};
struct POLYPOINT
{
	MYPOINT _point;
	TAG _tag;
	POLYPOINT();
	POLYPOINT(double pX, double pY, int pPolyTag, int pVertTag);
};

class POLYPOINTUCS : public POLYPOINT
{
public:
	double pathCost;
	TAG parentTag;
	POLYPOINTUCS();
	POLYPOINTUCS(double pX, double pY, int pPolyTag, int pVertTag, double pPathCost, TAG pParentTag);
};


class LineSegment;
class POLYGON
{
public:
	POLYGON();
	POLYGON(int numVert);
	POLYGON(std::initializer_list<POLYPOINT*> pointList);
	~POLYGON();

	void addVert(POLYPOINT* p);
	bool isCrossed(const LineSegment& line);
	int getNumVert(); // return number of vertices of this polygon
	POLYPOINT getPoint(int vertTag);
	bool isPtInside(const MYPOINT& pt);
private:
	std::vector <POLYPOINT*> _vertList;
	friend class LineSegment;
	friend class Map;
	friend class Algorithm;
};