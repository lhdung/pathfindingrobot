#include "stdafx.h"
#include "Algorithm.h"
#include "Map.h"
#include <list>
#include <vector>
#include <unordered_set>
#include <algorithm>
int Algorithm::IDAStar( Map & map,double& distance,std::vector<POLYPOINT*>& path)
{
	double cutOff = Distance(map._src._point, map._dest._point);
	path.push_back(&map._src);
	while (true)
	{
		double result = IDAStarSearch(path, 0, cutOff, map);
		if (result == FOUND)
		{
			int i;
			if (path.size() == 1)
				return FOUND;
			for (i = 0 ; i < path.size() - 1 ; ++i)
			{
				distance += Distance(path[i]->_point, path[i + 1]->_point);
			}
			return FOUND;
		}
		if (std::numeric_limits<double>::infinity() == result)
			return NOT_FOUND;
		cutOff = result;
	}
}

double Algorithm::IDAStarSearch(std::vector<POLYPOINT*>& path, double realCost, double cutOff, Map& map)
{
	auto node = path.back();
	double f = realCost + Distance(node->_point, map._dest._point);
	if (f > cutOff)
		return f;
	if (node->_point.x == map._dest._point.x && node->_point.y == map._dest._point.y)
		return FOUND;
	double min = std::numeric_limits<double>::infinity();
	auto childList = map.generateNodes(node);
	for (auto tag : *childList)
	{
		POLYPOINT* childNode = nullptr;
		if (tag.polyTag == map._polyList.size())
			childNode = &map._src;
		else if (tag.polyTag == map._polyList.size() + 1)
			childNode = &map._dest;
		else
			childNode = map._polyList[tag.polyTag]->_vertList[tag.vertTag];
		if (std::find(path.begin(), path.end(), childNode) == path.end())
		{
			path.push_back(childNode);
			double result = IDAStarSearch(path, realCost + Distance(childNode->_point, node->_point), cutOff, map);
			if (result == FOUND) return FOUND;
			if (result < min) min = result;
			path.pop_back();
		}
	}
	return min;
}




int Algorithm::AStar(Map & map, double & cost, std::vector<POLYPOINT*>& path)
{
	std::list<POLYPOINTUCS> frontiers;
	{
		// add source point to frontiers
		POLYPOINT point = map.getSource();
		frontiers.emplace_front(point._point.x, point._point.y, point._tag.polyTag, point._tag.vertTag, 0, TAG{ -1,-1 });
	}
	/* Hash table for explored nodes */
	static auto hash = [](POLYPOINTUCS a) -> size_t {return (a._tag.polyTag << 16) ^ a._tag.vertTag; };
	static auto eq = [](POLYPOINTUCS a, POLYPOINTUCS b) -> bool {return a._tag.polyTag == b._tag.polyTag && a._tag.vertTag==b._tag.vertTag; };
	std::unordered_set<POLYPOINTUCS, decltype(hash), decltype(eq)> exploredNode(10, hash, eq);
	


	std::vector<TAG> solution;


	while (!frontiers.empty())
	{
		/* Pop */
		auto minIter = std::min_element(frontiers.begin(), frontiers.end(), [&map](const POLYPOINTUCS& a, const POLYPOINTUCS& b) {
			return a.pathCost + Distance(a._point, map.getDest()._point) < b.pathCost + Distance(b._point, map.getDest()._point);
		});
		auto point = *minIter;
		frontiers.erase(minIter);


		// if the node is the goal node
		if (point._point.x == map.getDest()._point.x && point._point.y == map.getDest()._point.y)
		{
			// trackback (from des back to source) and and store the solution path
			cost = point.pathCost;
			path.push_back(map.getPointPtr(point._tag.polyTag, point._tag.vertTag));
			while (point.parentTag.polyTag != -1 && point.parentTag.vertTag != -1)
			{
				point._tag = point.parentTag;
				point = *exploredNode.find(point);
				path.push_back(map.getPointPtr(point._tag.polyTag, point._tag.vertTag));
			}
			// the order of elements in path is reversed now, so reverse them
			std::reverse(path.begin(), path.end());
			return FOUND;
		}


		/* if it's not the goal node */
		exploredNode.insert(point);

		// get list of candidates
		auto candidatesTag = map.generateNodes(&point);
		
		// for every candidate tag
		for (auto candidateTag: *candidatesTag)
		{
			POLYPOINT candidatePoint = map.getPoint(candidateTag.polyTag, candidateTag.vertTag);
			POLYPOINTUCS point(candidatePoint._point.x, candidatePoint._point.y, candidateTag.polyTag, candidateTag.vertTag, 
				point.pathCost + Distance({ point._point.x,point._point.y }, {candidatePoint._point.x,candidatePoint._point.y}),
				point._tag);

			// if it's in explored nodes, nevermind
			if (exploredNode.find(point) != exploredNode.end())
				continue;
			

			auto foundNode = std::find_if(frontiers.begin(), frontiers.end(), [candidateTag](const POLYPOINTUCS& a) {
				return a._tag.polyTag == candidateTag.polyTag && a._tag.vertTag == candidateTag.vertTag;
			});
			// if it's one of the frontiers with higher cost, replace it 
			// otherwise, put it in the frontiers list.
			if (foundNode == frontiers.end())
			{
				frontiers.push_back(point);
			}
			else
			{
				if (foundNode->pathCost > point.pathCost)
				{
					frontiers.erase(foundNode);
					frontiers.push_front(point);
				}

			}
		}


	}

	return NOT_FOUND;
}


int Algorithm::RecurBestFirstSearch(Map & map, double & distance, std::vector<POLYPOINT*>& path)
{
	path.push_back(&map._src);
	std::shared_ptr<POLYPOINTRBFS> node = std::shared_ptr<POLYPOINTRBFS>(new POLYPOINTRBFS);
	node->_polyPoint = &map._src;
	node->_f = Distance(map._src._point, map._dest._point);
	double result = RecurBFSUtil(path,node,0,std::numeric_limits<double>::infinity(),map);
	if (result == std::numeric_limits<double>::infinity())
		return NOT_FOUND;
	else if (result == FOUND)
	{
		int i;
		if (path.size() == 1)
			return FOUND;
		for (i = 0; i < path.size() - 1; ++i)
		{
			distance += Distance(path[i]->_point, path[i + 1]->_point);
		}
		return FOUND;
	}


}
double Algorithm::RecurBFSUtil(std::vector<POLYPOINT*>& path, std::shared_ptr<POLYPOINTRBFS>& node, double realCost, double bestValue, Map & map)
{
	if (node->_polyPoint->_point.x == map._dest._point.x && node->_polyPoint->_point.y == map._dest._point.y)
		return FOUND;
	auto childList = map.generateNodes(node->_polyPoint);
	std::vector<std::shared_ptr<POLYPOINTRBFS>> successorList;
	
	for (auto child : *childList)
	{
		std::shared_ptr<POLYPOINTRBFS> successor = std::shared_ptr<POLYPOINTRBFS>(new POLYPOINTRBFS);
		if (child.polyTag >= map._polyList.size())
		{
			successor->_polyPoint = &map._dest;
		}
		else
			successor->_polyPoint = map._polyList[child.polyTag]->_vertList[child.vertTag];
		successor->_f = std::max(node->_f, realCost + Distance(node->_polyPoint->_point, successor->_polyPoint->_point) + Distance(successor->_polyPoint->_point, map._dest._point));
		if (std::find_if(path.begin(), path.end(), [successor](POLYPOINT* polyPoint)-> bool {return successor->_polyPoint == polyPoint; })
			== path.end())
		{
			successorList.push_back(successor);
		}
		
	}
	if (successorList.empty())
		return std::numeric_limits<double>::infinity();
	while (true)
	{
		auto bestIter = std::min_element(successorList.begin(), successorList.end(), 
		[](std::shared_ptr<POLYPOINTRBFS>& i, std::shared_ptr<POLYPOINTRBFS>& j)-> bool
		{return i->_f < j->_f; });
		if ((*bestIter)->_f > bestValue)
		{
			return (*bestIter)->_f;
		}
		auto alterIter = std::min_element(successorList.begin(), successorList.end(),
			[bestIter](std::shared_ptr<POLYPOINTRBFS>& i, std::shared_ptr<POLYPOINTRBFS>& j)->bool
		{ if (i.get() == (*bestIter).get())
			return false;
		else
			return i->_f < j->_f; });
		path.push_back((*bestIter)->_polyPoint);
		(*bestIter)->_f = RecurBFSUtil(path, *bestIter, realCost + Distance(node->_polyPoint->_point, (*bestIter)->_polyPoint->_point), std::min(bestValue, (*alterIter)->_f),map);
		if ((*bestIter)->_f == FOUND)
			return FOUND;
		path.pop_back();
	}
}


