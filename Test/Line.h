#pragma once
#include "stdafx.h"
//Line equation format:
// Ax + By + C = 0
class Line
{
public:
	Line(const POINT& a, const POINT& b);
	Line();
protected:
	double _coefA;
	double _coefB;
	double _coefC;
};

class LineSegment : public Line
{
public:
	const POINT& getPointA();
	const POINT& getPointB();
	LineSegment();
	int getRelates(const LineSegment& line, POINT* & result);
	static LineSegment* createLineSegment(const POINT& a, const POINT& b);
private:
	POINT _a;
	POINT _b;
};