#include "stdafx.h"
#include "Line.h"
Line::Line(const POINT & a, const POINT & b)
{
	int vectX = -(b.y - a.y);
	int vectY = b.x - a.x;
	_coefA = vectX;
	_coefB = vectY;
	_coefC = vectX * (-a.x) + vectY * (-a.y);
}

Line::Line() : _coefA(),_coefB(),_coefC()
{
}
const POINT & LineSegment::getPointA()
{
	// TODO: insert return statement here
	return this->_a;
}
const POINT & LineSegment::getPointB()
{
	// TODO: insert return statement here
	return this->_b;
}
/*
	LineSegment default constructor
*/
LineSegment::LineSegment() : _a{}, _b{}
{
}
/*
	This function will check the relationship between two line segments
	Input: Another line segment you want to check
	Output:
		Return 0 if two lines segments intersects at only one point
		Return 1 if two lines are colinear and overlapped
		Return -1 if two lines doesn't fall in cases above
		Return the intersect point if any
*/
int LineSegment::getRelates(const LineSegment & line, POINT* & result)
{
	
	int crossProductA = CrossProduct(_a, _b, line._a, line._b);
	int crossProductB = CrossProduct(_a, line._a, _a, _b);
	if (crossProductA == 0 && crossProductB == 0)
	{
		double t0 = (double)DotProduct(_a, line._a, _a, _b)/ (double)DotProduct(_a,_b,_a,_b);
		POINT tmp{};
		double t1 = t0 +  (double)DotProduct(tmp, _a, _a, _b) /(double) DotProduct(_a, _b, _a, _b);
		if (DotProduct(tmp, _a, _a, _b) < 0)
		{
			if (t1 >= 0 && t1 < 1)
			{
				result = nullptr;
				return 1;
			}
			else if (t0 > 0 && t0 <= 1)
			{
				result = nullptr;
				return 1;
			}
			else if (t1 == 1)
			{
				result = new POINT();
				result->x = _b.x;
				result->y = _b.y;
				return 0;
			}
			else if (t0 == 0)
			{
				result = new POINT();
				result->x = _a.x;
				result->y = _a.y;
				return 0;
			}
			else
				return -1;
		}
		else
		{
			if (t0 >= 0 && t0 < 1)
			{
				result = nullptr;
				return 1;
			}
			else if (t1 > 0 && t1 <= 1)
			{
				result = nullptr;
				return 1;
			}
			else if (t0 == 1)
			{
				result = new POINT();
				result->x = _b.x;
				result->y = _b.y;
				return 0;
			}
			else if (t1 == 0)
			{
				result = new POINT();
				result->x = _a.x;
				result->y = _a.y;
				return 0;
			}
			else
				return -1;
		}
	}
	else if (crossProductA == 0 && crossProductB != 0)
	{
		return -1;
	}
	else if (crossProductA != 0)
	{
		double t = (double)CrossProduct(_a, line._a, line._a, line._b) / CrossProduct(_a, _b, line._a, line._b);
		double u = (double)CrossProduct(line._a, _a, _a, _b) / CrossProduct(line._a, line._b, _a, _b);
		if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
		{
			result->x = _a.x + t * (_b.x - _a.x);
			result->y = _a.y + t * (_b.y - _a.y);
			return 0;
		}
		return -1;
	}
	return -1;
}

/*
	This function will create a LineSegment from two points along with the line equation
	Input: POINT a, POINT b
	Output: The Line Segment, and its Line Equation, if error happens, nullptr will be returned
	P/s: The Line Segment is dynamically allocated, delete it if you finish.
*/
LineSegment* LineSegment::createLineSegment(const POINT & a, const POINT & b)
{
	if (a.x == b.x && a.y == b.y)
		return nullptr;
	LineSegment* lineSeg = new LineSegment;
	if (lineSeg == nullptr)
		return nullptr;
	lineSeg->_a = a;
	lineSeg->_b = b;
	int vectX = -(b.y - a.y);
	int vectY = b.x - a.x;
	lineSeg->_coefA = vectX;
	lineSeg->_coefB = vectY;
	lineSeg->_coefC = vectX * (-a.x) + vectY * (-a.y);
	return lineSeg;
}
