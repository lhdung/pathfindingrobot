// MapGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Geometry.h"
#include "Utility.h"
#include <fstream>
#include <forward_list>
#include <random>
#include <iostream>
const size_t DEFAULT_MAX_NUM_POLYGONS = 100;

std::ofstream logFile("logfile.txt");
std::random_device rd;
std::mt19937 mt(rd());


bool isPointInEllipse(const Point& point, const Ellipse& ellipse)
{
	
	const double& h = ellipse.center.x;
	const double& k = ellipse.center.y;
	const double& a = ellipse.semimajorAxis;
	const double& b = ellipse.semiminorAxis;

	return std::pow((point.x - h) / a, 2) + std::pow((point.y - k) / b, 2) <= 1;
}
// find if an ellipse intersects a line segment
bool isIntersect(const Ellipse& ellipse, const LineSegment& lineSegment)
{
	const double& x1 = lineSegment.p1.x;
	const double& x2 = lineSegment.p2.x;
	const double& y1 = lineSegment.p1.y;
	const double& y2 = lineSegment.p2.y;
	const double& h = ellipse.center.x;
	const double& k = ellipse.center.y;
	const float& a = ellipse.semimajorAxis;
	const float& b = ellipse.semiminorAxis;

	double A = a*a* (y2 - y1)*(y2 - y1) + b*b*(x2 - x1)*(x2 - x1);
	double B = 2 * b*b*(x2 - x1)*(x1 - h) + 2 * a*a*(y2 - y1)*(y1 - k);
	double C = b*b*(x1 - h)*(x1 - h) + a*a*(y1 - k)*(y1 - k) - a*a*b*b;

	double delta = B*B - 4 * A*C;
	if (delta < 0)
		return false;
	double deltaSquareRoot = sqrt(delta);

	double t = (-B - deltaSquareRoot) / (2 * A);
	if (t >= 0 && t <= 1)
		return true;

	t = (-B + deltaSquareRoot) / (2 * A);
	if (t >= 0 && t <= 1)
		return true;

	return false;
}

// return random ellipse from given metrics
Ellipse randEllipse(double maxEllipseSemiAxis, double minEllipseSemiAxis, const Rect& ellipsesCenterRange,
	std::tr1::uniform_real_distribution<double>& randPosGenH, std::tr1::uniform_real_distribution<double>& randPosGenV,
	std::tr1::uniform_real_distribution<double>& randEllipseSemiAxisGen)
{
	return { { randPosGenH(mt), randPosGenV(mt) }, randEllipseSemiAxisGen(mt), randEllipseSemiAxisGen(mt) };
}

LineSegment getNormalVector(const LineSegment& v)
{
	return { { v.p1.x + v.p1.y - v.p2.y },{ v.p1.y + v.p2.x - v.p1.x } };
}
double dotProduct(const LineSegment& v1, const LineSegment& v2)
{
	double x1 = v1.p2.x - v1.p1.x;
	double y1 = v1.p2.y - v1.p1.y;
	double x2 = v2.p2.x - v2.p1.x;
	double y2 = v2.p2.y - v2.p1.y;
	return x1*x2 + y1*y2;
}

// find if whether a given point is inside a polygon
// https://stackoverflow.com/questions/1119627/how-to-test-if-a-point-is-inside-of-a-convex-polygon-in-2d-integer-coordinates
bool isPointInsidePolygon(const Point& P, const Polygon& polygon)
{
	bool isInside = false;
	auto arr = polygon.vertices;
	for (auto i = arr.begin(), j = std::prev(arr.end()); i != arr.end(); j = i++)
	{
		if ((i->y <= P.y && j->y >= P.y)
			|| (i->y >= P.y &&j->y <= P.y))
		{
			auto slope = (j->x - i->x) / (j->y - i->y);
			auto ptX = i->x + slope*(P.y - i->y);
			if (ptX < P.x)
			{
				isInside = !isInside;
			}
			else if (ptX == P.x)
				return false;
		}
	}
	return isInside;
}
// Given an ellipse with center C and an angle
// Draw line CA with (CA, Ox) = angle
// Return point P at which CA and ellipse intersects
Point getPointOnEllipse(int angle, const Ellipse& ellipse)
{
	LineSegment line;
	line.p1 = ellipse.center;

	// Find CA and stored it in line
	if (angle == 90)
		line.p2 = { line.p1.x, line.p1.y + 10 };
	else if(angle == 270)
		line.p2 = { line.p1.x, line.p1.y - 10 };

	else
	{
		float slope = tan(toRadian(angle));
		double b = ellipse.center.y - ellipse.center.x * slope;
		if (angle >= 0 && angle < 90)
		{
			line.p2.x = line.p1.x + 10;
		}
		else if (angle > 90 && angle < 270)
		{
			line.p2.x = line.p1.x - 10;
		}
		else if (angle > 270 && angle <= 360)
		{
			line.p2.x = line.p1.x + 10;
		}
		line.p2.y = slope*line.p2.x + b;
	}

	// find point P
	const double& x1 = line.p1.x;
	const double& x2 = line.p2.x;
	const double& y1 = line.p1.y;
	const double& y2 = line.p2.y;
	const double& h = ellipse.center.x;
	const double& k = ellipse.center.y;
	const float& a = ellipse.semimajorAxis;
	const float& b = ellipse.semiminorAxis;

	double A = a*a* (y2 - y1)*(y2 - y1) + b*b*(x2 - x1)*(x2 - x1);
	double B = 2 * b*b*(x2 - x1)*(x1 - h) + 2 * a*a*(y2 - y1)*(y1 - k);
	double C = b*b*(x1 - h)*(x1 - h) + a*a*(y1 - k)*(y1 - k) - a*a*b*b;

	double delta = B*B - 4 * A*C;
	double deltaSquareRoot = sqrt(delta);
	// one of the two Ts
	double t1 = (-B - deltaSquareRoot) / (2 * A);
	double t2 = (-B + deltaSquareRoot) / (2 * A);
	double t;
	if (t1 >= 0)
		t = t1;
	else
		t = t2;
	// if the other T should be used
	auto x = x1 + (x2 - x1)*t;
	auto y = y1 + (y2 - y1)*t;


	// return P
	return { floor(x),floor(y) };
}

// Generate a random polygon out of an ellipse
Polygon randomPolygonFromEllipse(const Ellipse& ellipse)
{
	std::tr1::uniform_int_distribution<int> randStep(30, 120);

	Polygon randomPolygon;
	for (int i = 0; i < 360; i += randStep(mt))
	{

		randomPolygon.vertices.push_front(getPointOnEllipse(i, ellipse));
	}
	return randomPolygon;
}

void createMap(std::ofstream& file, const Rect& maxRect, double maxEllipseSemiAxis, double minEllipseSemiAxis,
	const Rect& ellipsesCenterRange, size_t numPolygons = 0)
{
	// if number of polygons is not specified, random it
	if (numPolygons == 0)
		std::tr1::uniform_int_distribution<int>(1, DEFAULT_MAX_NUM_POLYGONS)(mt);

	// use this to random ellipses' size
	std::tr1::uniform_real_distribution<double> randEllipseSemiAxisGen(minEllipseSemiAxis, maxEllipseSemiAxis);
	// use this to random x of a point
	std::tr1::uniform_real_distribution<double> randPosGenH(ellipsesCenterRange.TopLeft.x + maxEllipseSemiAxis, ellipsesCenterRange.BottomRight.x);
	// use this to random y of a point
	std::tr1::uniform_real_distribution<double> randPosGenV(ellipsesCenterRange.BottomRight.y + maxEllipseSemiAxis, ellipsesCenterRange.TopLeft.y);


	std::forward_list<Polygon> polygons; // polygons that have been generated
	size_t j;
	for (size_t i = 0,j = i; i < numPolygons; i++)
	{
		logFile << "--Start generating polygon indexed" << i << "\n";
		if(j==i)
			std::cout << "--Start generating polygon indexed" << i << "\n";
		j = i;
		Ellipse newEllipse;
		// keep randoming ellipse until its center is not inside any polygon
		while (newEllipse = randEllipse(maxEllipseSemiAxis, minEllipseSemiAxis, ellipsesCenterRange, randPosGenH,
			randPosGenV, randEllipseSemiAxisGen),
			[&newEllipse, &polygons]() {
			for (const auto& polygon : polygons)
			{
				if (isPointInsidePolygon(newEllipse.center, polygon))
					return true;
				for (const Point& p : polygon.vertices)
					if (isPointInEllipse(p, newEllipse))
						return true;
			}
			return false;
		}())
		{
			logFile << "Reject center point (" << newEllipse.center.x << "," << newEllipse.center.y << ")\n";
		}

		logFile << "Accept center point (" << newEllipse.center.x << "," << newEllipse.center.y << ")\n";

		std::list<LineSegment> intersectedSegments; // contains line segments that the new generated ellipse intersects
													// update intersectedSegments
		for (const auto& polygon : polygons)
		{
			for (auto iter = polygon.vertices.cbegin(); iter != polygon.vertices.cend(); ++iter)
				intersectedSegments.push_front({ { iter->x, iter->y },{ std::next(iter)->x, std::next(iter)->y } });
			intersectedSegments.push_front({ { polygon.vertices.back().x, polygon.vertices.back().y },
			{ polygon.vertices.front().x, polygon.vertices.front().y } });
		}

		bool generateSuccess = true;
		// while the ellipse still intersets at least 1 segment, reduce its size
		while (!intersectedSegments.empty())
		{
			// if the semimajorAxis has not reached min value yet, reduce it
			// if the semiminorAxis has not reached min value yet, reduce it
			// if both reach min value, start over (randomize another ellipse)
			if (newEllipse.semimajorAxis > minEllipseSemiAxis)
			{
				--newEllipse.semimajorAxis;
				if (newEllipse.semiminorAxis > minEllipseSemiAxis)
					--newEllipse.semiminorAxis;
			}
			else
			{
				if (newEllipse.semiminorAxis > minEllipseSemiAxis)
					--newEllipse.semiminorAxis;
				else
				{
					--i;
					generateSuccess = false;
					break;
				}
			}
			intersectedSegments.remove_if([&newEllipse](const LineSegment& sm) {
				return !isIntersect(newEllipse, sm);
			});
		}

		// if successully generated an ellipse, generate a random polygon from it
		if (generateSuccess)
		{
			logFile << "Generate polygon successfully\n";
			polygons.push_front(randomPolygonFromEllipse(newEllipse));
		}
		else
			logFile << "Fail to generate polygon\n";
		++j;
	}
	//Generate Source and Destination
	std::uniform_int_distribution<int> randPtX((int)maxRect.TopLeft.x, (int)maxRect.BottomRight.x);
	std::uniform_int_distribution<int> randPtY((int)maxRect.BottomRight.y, (int)maxRect.TopLeft.y);
	Point pt;
	Polygon poly;
	bool isSatisfied = false;
	do
	{
		isSatisfied = true;
		pt.x = randPtX(mt);
		pt.y = randPtY(mt);
		for (auto iter = polygons.begin(); iter != polygons.end(); ++iter)
		{
			if (isPointInsidePolygon(pt, *iter))
			{
				isSatisfied = false;
				break;
			}
		}
		if (isSatisfied)
			break;
	} while (true);
	file << "(" << pt.x << ";" << pt.y << ")\n";

	do
	{
		isSatisfied = true;
		pt.x = randPtX(mt);
		pt.y = randPtY(mt);
		for (auto iter = polygons.begin(); iter != polygons.end(); ++iter)
		{
			if (isPointInsidePolygon(pt, *iter))
			{
				isSatisfied = false;
				break;
			}
		}
		if (isSatisfied)
			break;
	} while (true);
	// write polygons to file
	file << "(" << pt.x << ";" << pt.y << ")";
	for (const auto& polygon : polygons)
	{
		file << "\n";
		for (const auto& point : polygon.vertices)
			file << point;
	}
	file << "\n";

}
int main()
{
	std::ofstream file("autoGeneratedInput.txt");
	int left{};
	int right{};
	int bottom{};
	int top{};
	int minAxis{};
	int maxAxis{};
	int minXCen{};
	int maxXCen{};
	int minYCen{};
	int maxYCen{};
	int numOfPoly{};
	std::cout << "Enter the Range of X coordinates that you want your polygon is placed: ";
	std::cin >> left >> right;
	std::cout << "Enter the Range of Y coordinates that you want your polygon is placed: ";
	std::cin >> bottom >> top;
	std::cout << "Enter the minimum and maximum size of your polygons: ";
	std::cin >> minAxis >> maxAxis;
	std::cout << "Enter the number of polygons you want to generate: ";
	std::cin >> numOfPoly;
	createMap(file, { { (double)left,(double)top },{ (double)right,(double)bottom } }, (double)maxAxis, (double)minAxis, { { (double)left,(double)top },{ (double)right,(double)bottom } }, numOfPoly);
	//createMap(file, { { 0,1000 },{ 1000,0 } }, 250, 60, { { 0,1000 },{ 1000,0 } }, 3);
	/*Polygon pol{ { {215, 345},{141, 318},{110, 347},{95, 395},{118, 458},{166, 470},{223, 392} } };
	Point p{ 138,443 };
	std::cout << std::boolalpha << isPointInsidePolygon(p, pol);*/
	std::cout << "--------------------Sucessful Generating Polygons---------------\n";
	std::cin.get();
	return 0;
}

